﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tilemap Tools/Tilemap")]
public class CustomTilemap : ScriptableObject
{
    public string levelName;

    [SerializeField]
    public List<Tile> map = new List<Tile>(100);

    public string GetLevelName() {
        if (levelName == "") {
            return "DefaultLevel.json";
        }
        return levelName + ".json";
    }
}
