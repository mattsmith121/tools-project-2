﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameScene/Factory")]
public class Factory : ScriptableObject
{
    public int spawnRate;
    public int health;
    public SpriteComponent spriteComponent;

    public bool Complete() {
        return spriteComponent != null;
    }

    public string GetJSON(Vector2 position) {
        string json = "{";
        json += "\"name\": \"Factory\",";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"position\": { \"x\": " + string.Format("{0:0.00}", position.x) + ", \"y\": " + string.Format("{0:0.00}", position.y) + "},";
        json += "\"components\": [";
        json += GetFactoryComponent() + ",";
        json += GetEnemyComponent() + ",";
        json += spriteComponent.GetJSON();
        json += "]";
        json += "}";
        return json;
    }

    private string GetEnemyComponent() {
        string json = "{";
        json += "\"className\": \"Enemy\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"health\": " + health;
        json += "}";
        return json;
    }

    private string GetFactoryComponent() {
        string json = "{";
        json += "\"className\": \"Factory\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"spawnRate\": " + spawnRate;
        json += "}";
        return json;
    }
}
