﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameScene/Potion")]
public class Potion : ScriptableObject
{
    public int type;
    public SpriteComponent spriteComponent;

    public bool Complete() {
        return spriteComponent != null;
    }

    private string GetPotionName() {
        if (type == 0) {
            return "HealthPotion";
        } else {
            return "Antidote";
        }
    }

    public string GetJSON(Vector2 position) {
        string json = "{";
        json += "\"name\": \"" + GetPotionName() + "\",";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"position\": { \"x\": " + string.Format("{0:0.00}", position.x) + ", \"y\": " + string.Format("{0:0.00}", position.y) + "},";
        json += "\"components\": [";
        json += GetPotionComponent() + ",";
        json += spriteComponent.GetJSON();
        json += "]";
        json += "}";
        return json;
    }

    private string GetPotionComponent() {
        string json = "{";
        json += "\"className\": \"Potion\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"type\": " + type;
        json += "}";
        return json;
    }
}
