﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameScene/ExitDoor")]
public class ExitDoor : ScriptableObject
{
    public SpriteComponent spriteComponent;

    public bool Complete() {
        return spriteComponent != null;
    }

    public string GetJSON(Vector2 position) {
        string json = "{";
        json += "\"name\": \"ExitDoor\",";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"position\": { \"x\": " + string.Format("{0:0.00}", position.x) + ", \"y\": " + string.Format("{0:0.00}", position.y) + "},";
        json += "\"components\": [";
        json += GetExitDoorComponent() + ",";
        json += spriteComponent.GetJSON();
        json += "]";
        json += "}";
        return json;
    }

    private string GetExitDoorComponent() {
        string json = "{";
        json += "\"className\": \"ExitDoor\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\"";
        json += "}";
        return json;
    }
}
