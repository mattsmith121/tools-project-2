﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tilemap Tools/Tile")]
public class Tile : ScriptableObject
{
    public int ID;
    public Sprite Texture;
    public bool WallTop = false;
    public bool WallBottom = false;
    public bool WallRight = false;
    public bool WallLeft = false;
}
