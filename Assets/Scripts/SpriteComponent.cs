﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Components/Sprite")]
public class SpriteComponent : ScriptableObject
{
    public Sprite Texture;
    public Vector2 scale;
    public bool originCenter;
    public bool centerOfTile;
    public bool useTileCoordinate;
    public Color color;

    public string GetJSON() {
        // Get texture name
        string spriteName = System.IO.Path.GetFileName(AssetDatabase.GetAssetPath(Texture));
        spriteName = "../Assets/Textures/" + spriteName;

        string json = "{";
        json += "\"className\": \"Sprite\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"texture_filename\": \"" + spriteName + "\",";
        json += "\"scale\": { \"x\": " + string.Format("{0:0.00}", scale.x)  + ", \"y\": " + string.Format("{0:0.00}", scale.y) + "},";
        json += "\"originCenter\": " + originCenter.ToString().ToLower() + ",";
        json += "\"centerOfTile\": " + centerOfTile.ToString().ToLower() + ",";
        json += "\"useTileCoordinates\": " + useTileCoordinate.ToString().ToLower() + ",";
        json += "\"color\": { \"r\": " + color.r * 255f + ", \"g\": " + color.g * 255f + ", \"b\": " + color.b * 255f + "}";
        json += "}";

        return json;
    }
}
