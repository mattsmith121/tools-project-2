﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameScene/Game Scene")]
public class GameScene : ScriptableObject
{
    public string sceneName;

    public Potion[] potions = new Potion[100];
    public Factory[] factories = new Factory[100];

    public CustomTilemap tilemap;
    public Player player;
    public Vector2 playerPosition;
    public ExitDoor exitDoor;
    public Vector2 exitDoorPosition;
}
