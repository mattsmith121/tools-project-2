﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameScene/Player")]
public class Player : ScriptableObject
{
    public int health;
    public SpriteComponent spriteComponent;

    public bool Complete() {
        return spriteComponent != null;
    }

    public string GetJSON(Vector2 position) {
        string json = "{";
        json += "\"name\": \"Player\",";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"position\": { \"x\": " + string.Format("{0:0.00}", position.x) + ", \"y\": " + string.Format("{0:0.00}", position.y) + "},";
        json += "\"components\": [";
        json += GetPlayerComponent() + ",";
        json += spriteComponent.GetJSON();
        json += "]";
        json += "}";
        return json;
    }

    private string GetPlayerComponent() {
        string json = "{";
        json += "\"className\": \"Player\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"health\": " + health;
        json += "}";
        return json;
    }
}
