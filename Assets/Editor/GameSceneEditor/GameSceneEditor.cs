using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;
using System;

public class GameSceneEditor : EditorWindow
{
    [MenuItem("Window/Project/GameSceneEditor")]
    public static void ShowExample()
    {
        GameSceneEditor wnd = GetWindow<GameSceneEditor>();
        wnd.titleContent = new GUIContent("GameSceneEditor");
    }

    struct Coords
    {
        public int x;
        public int y;

        public Coords(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    enum EditMode
    {
        Paint,
        Delete
    }

    GameScene gameScene;
    TextField gameSceneNameField;
    ObjectField tilemapObjectField;
    ObjectField playerObjectField;
    Vector2Field playerCoords; // TODO: initialize with gamescene data
    ObjectField exitDoorObjectField;
    Vector2Field exitDoorCoords; // TODO: initialize with gamescene data
    VisualElement right_panel;
    VisualElement left_panel;

    // Factory stuff
    Factory[] factories;
    Factory selectedFactory;
    ListView factoryListView;

    // Potion Stuff
    Potion[] potions;
    Potion selectedPotion;
    ListView potionListView;

    bool flag = false;

    Button addItemButton;
    Button deleteItemButton;
    bool addMode = true;

    public void OnEnable() {
        // Game scene starts null (to clear any leftover game scene)
        gameScene = null;

        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement main_panel = new VisualElement();
        main_panel.style.flexGrow = 1.0f;
        main_panel.style.flexDirection = FlexDirection.Row;
        main_panel.style.borderTopWidth = main_panel.style.borderRightWidth = main_panel.style.borderBottomWidth = main_panel.style.borderLeftWidth = 5.0f;
        main_panel.style.borderTopColor = main_panel.style.borderRightColor = main_panel.style.borderBottomColor = main_panel.style.borderLeftColor = Color.black;
        root.Add(main_panel);

        #region Left Panel Root
        left_panel = new VisualElement();
        left_panel.style.flexDirection = FlexDirection.Column;
        left_panel.style.flexGrow = 0.25f;
        //left_panel.style.minWidth = 150f;
        left_panel.style.borderTopWidth = left_panel.style.borderRightWidth = left_panel.style.borderBottomWidth = left_panel.style.borderLeftWidth = 5.0f;
        left_panel.style.borderTopColor = left_panel.style.borderRightColor = left_panel.style.borderBottomColor = left_panel.style.borderLeftColor = Color.blue;
        main_panel.Add(left_panel);

        // GameScene
        ObjectField gameSceneObjectField = new ObjectField("Game Scene");
        gameSceneObjectField.objectType = typeof(GameScene);
        gameSceneObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnGameSceneChanged);
        left_panel.Add(gameSceneObjectField);

        gameSceneNameField = new TextField();
        gameSceneNameField.label = "Scene Name:";
        gameSceneNameField.RegisterCallback<ChangeEvent<string>>(OnSceneNameChanged);
        left_panel.Add(gameSceneNameField);

        // Tilemap
        tilemapObjectField = new ObjectField("Tilemap");
        tilemapObjectField.objectType = typeof(CustomTilemap);
        tilemapObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnTilemapChanged);
        left_panel.Add(tilemapObjectField);

        // Player
        playerObjectField = new ObjectField("Player");
        playerObjectField.objectType = typeof(Player);
        playerObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnPlayerChanged);
        left_panel.Add(playerObjectField);

        playerCoords = new Vector2Field("Player Position");
        playerCoords.RegisterCallback<ChangeEvent<Vector2>>(OnPlayerCoordsChanged);
        left_panel.Add(playerCoords);

        // ExitDoor
        exitDoorObjectField = new ObjectField("ExitDoor");
        exitDoorObjectField.objectType = typeof(ExitDoor);
        exitDoorObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnExitDoorChanged);
        left_panel.Add(exitDoorObjectField);

        exitDoorCoords = new Vector2Field("Player Position");
        exitDoorCoords.RegisterCallback<ChangeEvent<Vector2>>(OnExitDoorCoordsChanged);
        left_panel.Add(exitDoorCoords);

        addItemButton = new Button() {
            text = "Add Item",
            style = {
                width = 100,
                height = 50
            }
        };
        addItemButton.clicked += () => SwitchMode(true);
        left_panel.Add(addItemButton);

        deleteItemButton = new Button() {
            text = "Delete Item",
            style = {
                width = 100,
                height = 50
            }
        };
        deleteItemButton.clicked += () => SwitchMode(false);
        left_panel.Add(deleteItemButton);

        // Factories
        Label factoryLabel = new Label("Factories:");
        left_panel.Add(factoryLabel);
        SetFactoryList();

        // Potion
        Label potionLabel = new Label("Potions:");
        left_panel.Add(potionLabel);
        SetPotionList();

        // Export Button
        Button exportGameScene = new Button() {
            text = "Export Game Scene",
            style = {
                width = 200,
                height = 50
            }
        };
        exportGameScene.clicked += () => ExportGameScene();
        left_panel.Add(exportGameScene);

        #endregion

        #region Right Panel Root
        right_panel = new VisualElement();
        right_panel.style.flexDirection = FlexDirection.Column;
        right_panel.style.flexGrow = 1.0f;
        right_panel.style.borderTopWidth = right_panel.style.borderRightWidth = right_panel.style.borderBottomWidth = right_panel.style.borderLeftWidth = 5.0f;
        right_panel.style.borderTopColor = right_panel.style.borderRightColor = right_panel.style.borderBottomColor = right_panel.style.borderLeftColor = Color.red;
        main_panel.Add(right_panel);
        #endregion

        SetTiles();

        // default to add mode
        SwitchMode(true);
    }

    private void SetTiles() {
        // Clear panel first
        right_panel.Clear();

        // Fill Tiles
        for (int j = 0; j < 10; j++) {
            VisualElement image_row_panel = new VisualElement();
            image_row_panel.style.flexDirection = FlexDirection.Row;
            right_panel.Add(image_row_panel);

            for (int i = 0; i < 10; i++) {
                Box box = new Box();
                box.style.minWidth = 64;
                box.style.minHeight = 64;
                box.style.maxWidth = 64;
                box.style.maxHeight = 64;
                box.style.paddingBottom = box.style.paddingTop = box.style.paddingRight = box.style.paddingLeft = 1f;
                box.style.borderBottomColor = box.style.borderTopColor = box.style.borderRightColor = box.style.borderLeftColor = Color.white;
                box.RegisterCallback<MouseUpEvent, Coords>(OnMouseUpEvent_Image, new Coords(i, j));
                image_row_panel.Add(box);

                Image image = new Image();
                image.style.flexGrow = 1.0f;
                SetImageSprite(image, i, j);
                box.Add(image);

                Image thing = new Image();
                thing.style.flexGrow = 1.0f;
                if (gameScene == null) {
                    // Have to do this or nothing shows
                    thing.sprite = Resources.Load<Sprite>("Blank");
                } else if (gameScene.factories[j*10 + i] != null &&
                    gameScene.factories[j * 10 + i].spriteComponent != null) {
                    thing.sprite = gameScene.factories[j * 10 + i].spriteComponent.Texture;
                } else if (gameScene.potions[j * 10 + i] != null &&
                    gameScene.potions[j * 10 + i].spriteComponent != null) {
                    thing.sprite = gameScene.potions[j * 10 + i].spriteComponent.Texture;
                } else {
                    // Have to do this or nothing shows
                    thing.sprite = Resources.Load<Sprite>("Blank");
                }
                image.Add(thing);
            }
        }
    }

    private void OnGameSceneChanged(ChangeEvent<UnityEngine.Object> evt) {
        gameScene = evt.newValue as GameScene;
        if (gameScene == null) {
            tilemapObjectField.value = null;
            playerObjectField.value = null;
            exitDoorObjectField.value = null;
            exitDoorCoords.value = new Vector2(0, 0);
            playerCoords.value = new Vector2(0, 0);
            gameSceneNameField.value = "";
        } else {
            tilemapObjectField.value = gameScene.tilemap;
            playerObjectField.value = gameScene.player;
            exitDoorObjectField.value = gameScene.exitDoor;
            exitDoorCoords.value = gameScene.exitDoorPosition;
            playerCoords.value = gameScene.playerPosition;
            gameSceneNameField.value = gameScene.sceneName;
            EditorUtility.SetDirty(gameScene);
        }

        SetTiles();
    }

    private void OnMouseUpEvent_Image(MouseUpEvent _event, Coords pos) {
        if (gameScene == null)
            return;
        int x = pos.x;
        int y = pos.y;
        Sprite s = null;

        if (addMode) {
            if (selectedFactory != null) {
                gameScene.factories[y * 10 + x] = selectedFactory;
                EditorUtility.SetDirty(gameScene);
                s = selectedFactory.spriteComponent.Texture;
            } else if (selectedPotion != null) {
                gameScene.potions[y * 10 + x] = selectedPotion;
                EditorUtility.SetDirty(gameScene);
                s = selectedPotion.spriteComponent.Texture;
            } else {
                return;
            }
        } else { // delete mode
            gameScene.factories[y * 10 + x] = null;
            gameScene.potions[y * 10 + x] = null;
            s = Resources.Load<Sprite>("Blank");
        }

        Image image = _event.target as Image;
        if (image != null) {
            image.sprite = s;
        }

        
    }

    private void OnTilemapChanged(ChangeEvent<UnityEngine.Object> evt) {
        if (gameScene != null) {
            gameScene.tilemap = evt.newValue as CustomTilemap;
            EditorUtility.SetDirty(gameScene);
        }

        SetTiles();
    }

    private void OnPlayerChanged(ChangeEvent<UnityEngine.Object> evt) {
        if (gameScene != null) {
            gameScene.player = evt.newValue as Player;
            EditorUtility.SetDirty(gameScene);
        }
    }

    private void OnPlayerCoordsChanged(ChangeEvent<Vector2> evt) {
        if (gameScene == null) {
            return;
        }
        float x = 0;
        float y = 0;
        if (evt.newValue.x < 0) {
            x = 0;
        } else if (evt.newValue.x >= 10) {
            x = 9;
        } else {
            x = evt.newValue.x;
        }
        if (evt.newValue.y < 0) {
            y = 0;
        }
        else if (evt.newValue.y >= 10) {
            y = 9;
        } else {
            y = evt.newValue.y;
        }
        playerCoords.value = new Vector2(x, y);
        gameScene.playerPosition = new Vector2(x, y);
        EditorUtility.SetDirty(gameScene);
    }

    private void OnExitDoorChanged(ChangeEvent<UnityEngine.Object> evt) {
        if (gameScene != null) {
            gameScene.exitDoor = evt.newValue as ExitDoor;
            EditorUtility.SetDirty(gameScene);
        }
    }

    private void OnExitDoorCoordsChanged(ChangeEvent<Vector2> evt) {
        if (gameScene == null) {
            return;
        }
        float x = 0;
        float y = 0;
        if (evt.newValue.x < 0) {
            x = 0;
        }
        else if (evt.newValue.x >= 10) {
            x = 9;
        }
        else {
            x = evt.newValue.x;
        }
        if (evt.newValue.y < 0) {
            y = 0;
        }
        else if (evt.newValue.y >= 10) {
            y = 9;
        }
        else {
            y = evt.newValue.y;
        }
        exitDoorCoords.value = new Vector2(x, y);
        gameScene.exitDoorPosition = new Vector2(x, y);
        EditorUtility.SetDirty(gameScene);
    }

    private void OnSceneNameChanged(ChangeEvent<string> evt) {
        if (gameScene == null) {
            return;
        }
        gameScene.sceneName = evt.newValue;
        EditorUtility.SetDirty(gameScene);
    }

    private void SetImageSprite(Image image, int x, int y) {
        if (gameScene == null) {
            image.sprite = null;
            return;
        }

        CustomTilemap tilemap = gameScene.tilemap;
        if (tilemap == null || tilemap.map[y * 10 + x] == null || tilemap.map[y * 10 + x].Texture == null) {
            image.sprite = null;
        }
        else {
            image.sprite = tilemap.map[y * 10 + x].Texture;
        }
    }

    private void SetFactoryList() {
        // Create some list of data
        factories = Resources.LoadAll<Factory>("Factories");

        // The "makeItem" function will be called as needed
        // when the ListView needs more items to render
        Func<VisualElement> makeItem = () => {
            Image image = new Image();
            image.style.width = 50f;
            image.style.height = 50f;
            image.style.paddingTop = image.style.paddingBottom = image.style.paddingRight = image.style.paddingLeft = 5f;
            image.styleSheets.Add(Resources.Load<StyleSheet>("SelectedStyle"));
            return image;
        };

        // As the user scrolls through the list, the ListView object
        // will recycle elements created by the "makeItem"
        // and invoke the "bindItem" callback to associate
        // the element with the matching data item (specified as an index in the list)
        Action<VisualElement, int> bindItem = (e, i) => (e as Image).sprite = factories[i].spriteComponent.Texture;

        // Provide the list view with an explict height for every row
        // so it can calculate how many items to actually display
        const int itemHeight = 50;

        factoryListView = new ListView(factories, itemHeight, makeItem, bindItem);

        factoryListView.onSelectionChange += objects => {
            // Clear potion stuff
            if (selectedPotion) {
                selectedPotion = null;
                potionListView.ClearSelection();
            }

            // Set selected tile to selected item
            selectedFactory = factoryListView.selectedItem as Factory;
        };

        var ht = factoryListView.Children();
        factoryListView.selectionType = SelectionType.Single;
        factoryListView.style.flexGrow = 1.0f;
        left_panel.Add(factoryListView);
    }

    private void SetPotionList() {
        // Create some list of data
        potions = Resources.LoadAll<Potion>("Potions");

        // The "makeItem" function will be called as needed
        // when the ListView needs more items to render
        Func<VisualElement> makeItem = () => {
            Image image = new Image();
            image.style.width = 50f;
            image.style.height = 50f;
            image.style.paddingTop = image.style.paddingBottom = image.style.paddingRight = image.style.paddingLeft = 5f;
            image.styleSheets.Add(Resources.Load<StyleSheet>("SelectedStyle"));
            return image;
        };

        // As the user scrolls through the list, the ListView object
        // will recycle elements created by the "makeItem"
        // and invoke the "bindItem" callback to associate
        // the element with the matching data item (specified as an index in the list)
        Action<VisualElement, int> bindItem = (e, i) => (e as Image).sprite = potions[i].spriteComponent.Texture;

        // Provide the list view with an explict height for every row
        // so it can calculate how many items to actually display
        const int itemHeight = 50;

        potionListView = new ListView(potions, itemHeight, makeItem, bindItem);

        potionListView.onSelectionChange += objects => {
            // Clear factory stuff
            if (selectedFactory) {
                selectedFactory = null;
                factoryListView.ClearSelection();
            }
            // Set selected tile to selected item
            selectedPotion = potionListView.selectedItem as Potion;
        };

        var ht = potionListView.Children();
        potionListView.selectionType = SelectionType.Single;
        potionListView.style.flexGrow = 1.0f;
        left_panel.Add(potionListView);
    }

    private void SwitchMode(bool add) {
        addItemButton.style.color = add ? Color.yellow : Color.grey;
        deleteItemButton.style.color = !add ? Color.yellow : Color.grey;
        addMode = add;
    }

    private void ExportGameScene() {
        if (gameScene == null) {
            return;
        }

        string gameSceneJSON = "{ \"GameObjects\": [";

        // Save game manager
        // this is done automatically since the only dynamic field is the level name from the tilemap
        if (gameScene.tilemap == null) {
            Debug.LogError("Can't save game scene, no tilemap loaded.");
            return;
        }
        gameSceneJSON += GameManagerJSON();

        gameSceneJSON += ",";

        // Save player
        if (gameScene.player == null && gameScene.player.Complete()) {
            Debug.LogError("Can't save game scene, no player loaded or player missing data.");
            return;
        }
        gameSceneJSON += gameScene.player.GetJSON(playerCoords.value);

        gameSceneJSON += ",";

        // Save Exit Door
        if (gameScene.exitDoor == null && gameScene.exitDoor.Complete()) {
            Debug.LogError("Can't save game scene, no exit door loaded or exit door missing data.");
            return;
        }
        gameSceneJSON += gameScene.exitDoor.GetJSON(exitDoorCoords.value);


        gameSceneJSON += ",";

        // Save factories
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                if (gameScene.factories[y*10 + x] != null && gameScene.factories[y * 10 + x].Complete()) {
                    gameSceneJSON += gameScene.factories[y * 10 + x].GetJSON(new Vector2(x,y));
                    gameSceneJSON += ",";
                }
            }
        }

        // Save potions
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                if (gameScene.potions[y * 10 + x] != null && gameScene.potions[y * 10 + x].Complete()) {
                    gameSceneJSON += gameScene.potions[y * 10 + x].GetJSON(new Vector2(x, y));
                    gameSceneJSON += ",";
                }
            }
        }

        // Get final ui json, this is always the same
        gameSceneJSON += GetUIJSON();

        // Close up json
        gameSceneJSON += "]}";

        // Get name for file
        string sceneName = "default";
        if (gameSceneNameField.value != "") {
            sceneName = gameSceneNameField.value;
        }

        // Write to file
        System.IO.File.WriteAllText(Application.dataPath + "/Exports/" + sceneName + ".json", gameSceneJSON);
    }

    private string GameManagerJSON() {
        string json = "{";
        json += "\"name\": \"GameManager\",";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"position\": { \"x\": " + 0 + ", \"y\": " + 0 + "},";
        json += "\"components\": [{";
        json += "\"className\": \"GameManager\",\n";
        json += "\"uid\": \"" + System.Guid.NewGuid() + "\",";
        json += "\"DefaultLevel\": \"../Assets/" + gameScene.tilemap.GetLevelName() + "\"";
        json += "}]";
        json += "}";
        return json;
    }

    private string GetUIJSON() {
        return @"{
			""name"": ""HealthText"",
            ""uid"": ""fbc30136-1342-4ab6-9fce-6c8551d98e30"",
			""position"": {
                 ""x"": 75.0,
				""y"": 655.0
            },
			""components"": [
                {
                    ""className"": ""TextSprite"",
					""uid"": ""5ced8724-2007-435f-ae04-230c98203341"",
					""text"": ""Health: 100"",
					""fontSize"": 16
                }
			]
		},
		{
			""name"": ""HealthPotionText"",
			""uid"": ""1937a71c-43eb-4771-a5a2-ae0bc0f8c257"",
			""position"": {
				""x"": 265.0,
				""y"": 655.0
			},
			""components"": [
				{
					""className"": ""TextSprite"",
					""uid"": ""85cf69d5-44f8-4e4c-8d19-efa7f301debc"",
					""text"": ""Health Potions: 0"",
					""fontSize"": 16

                }
			]
		},
		{
            ""name"": ""AntidoteText"",
			""uid"": ""c73c0d51 -ad0a-425e-a62d-b05641b5781d"",
			""position"": {
                ""x"": 475.0,
				""y"": 655.0

            },
			""components"": [
                {
                    ""className"": ""TextSprite"",
					""uid"": ""bbd33927 -9884-4cf5-9699-de10453b45c5"",
                    ""text"": ""Antidotes: 0"",
					""fontSize"": 16
                }
			]
        }";
    }
}